Vue.component('modal', {
    data: function () {
        return {
            sender: this.$parent.$data.publicKey || '',
            recipient: '',
            amount: 0,
            privateKey: ''
        }
    },
    methods: {
        sendTransaction: function () {
            let sJWT = KJUR.jws.JWS.sign('RS256', this.getHeader(), this.getBody(), this.getPrivateKey());

            $.ajax({
                url: 'http://0.0.0.0:5000/transactions/new',
                data: {'request': sJWT},
                type: "POST",
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                },
                success: function (result) {
                    alert(result.message);
                },
                error: function (error) {
                    alert(error.message || 'An error occured.');
                }
            });
        },

        getHeader: function () {
            return JSON.stringify({
                'alg': 'RS256',
                'typ': 'JWT',
                'public_key': this.sender
            });
        },

        getBody: function () {
            return JSON.stringify({
                'address': this.recipient,
                'amount': this.amount
            });
        },

        getPrivateKey: function () {
            return '-----BEGIN RSA PRIVATE KEY-----' + atob(this.privateKey) + '-----END RSA PRIVATE KEY-----';
        }
    },
    template: `<transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">

                    <div class="modal-header">
                        <h3>New transaction</h3>
                    </div>

                    <div class="modal-body">
                        <input class="form-control mb-3" type="text" v-model="sender" placeholder="Public key">
                        <input class="form-control mb-3" type="text" v-model="recipient" placeholder="Recipient">
                        <input class="form-control mb-3" type="number" min="0" v-model="amount">
                        <input class="form-control mb-3" type="text" v-model="privateKey" placeholder="Private key">
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-success mr-auto" v-on:click="sendTransaction">
                            Send
                        </button>
                        
                        <button class="btn btn-danger" v-on:click="$emit('close')">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </transition>`
});

Vue.component('balance', {
    data: function () {
        return {
            walletAddress: this.$parent.$data.walletAddress || '',
            balance: null
        }
    },
    methods: {
        checkBalance: function () {
            $.ajax({
                url: 'http://0.0.0.0:5000/balance?public_key=' + this.walletAddress,
                type: "POST",
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                },
                success: function (result) {
                    alert('Your current balance is: ' + result.balance);
                },
                error: function (error) {
                    alert(error.message || 'An error occured.');
                }
            });
        },
    },
    template: `<transition name="balance">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">

                    <div class="modal-header">
                        <h3>New transaction</h3>
                    </div>

                    <div class="modal-body">
                        <input class="form-control mb-3" type="text" v-model="walletAddress" placeholder="Wallet address">
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-success mr-auto" v-on:click="checkBalance">
                            Check
                        </button>
                        
                        <button class="btn btn-danger" v-on:click="$emit('close')">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </transition>`
});

new Vue({
    el: '#app',
    data: {
        privateKey: null,
        publicKey: null,
        showModal: false,
        balance: false,
    },
    methods: {
        getKeys: function () {
            let preloader = $("#preloder");
            let loader = $(".loader");

            preloader.fadeIn();
            loader.fadeIn();

            let crypt = new JSEncrypt({default_key_size: 1024});

            this.privateKey = btoa(
                crypt.getPrivateKey()
                    .replace('-----BEGIN RSA PRIVATE KEY-----', '')
                    .replace('-----END RSA PRIVATE KEY-----', '')
            );

            this.publicKey = btoa(
                crypt.getPublicKey()
                    .replace('-----BEGIN PUBLIC KEY-----', '')
                    .replace('-----END PUBLIC KEY-----', '')
            );

            this.walletAddress = sha256(this.publicKey);

            preloader.fadeOut("slow");
        }
    }
});
